ARG BASE_IMAGE
ARG BASE_IMAGE_TAG
FROM ${BASE_IMAGE}:${BASE_IMAGE_TAG}
ARG BASE_IMAGE
ARG BASE_IMAGE_TAG

ENV BASE_IMAGE=${BASE_IMAGE}
RUN echo "Building for ${BASE_IMAGE}:${BASE_IMAGE_TAG}"

RUN if [ "${BASE_IMAGE}" = "alpine" ]; then \
  apk add bash git jq yq curl wget; \
  fi

RUN if [ "${BASE_IMAGE}" = "ubuntu" ]; then \
  apt-get update -qq && apt-get install -y bash git jq wget curl && \
  wget https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 -O /usr/bin/yq && \
  chmod +x /usr/bin/yq; \
  fi

COPY --chmod=755 scripts/gitlab-trigger-deploy.sh /usr/bin/gitlab-trigger-deploy
COPY --chmod=755 scripts/git-versions-deploy.sh /usr/bin/git-versions-deploy

#!/bin/bash

# debug capabilities
if [ -n "$DEBUG" ]; then
  set -x
fi

# enable strict mode
set -euo pipefail

#################
# CONFIGURATION #
#################
: ${DEPLOY_GIT_EMAIL:="ci@example.com"}
: ${DEPLOY_GIT_NAME:="ci"}
: ${DEPLOY_GIT_VERSIONS_REPO_PROTO:="https"} # for not only http or https allowed
: ${DEPLOY_GIT_VERSIONS_REPO_USERNAME:="oauth2"}
: ${DEPLOY_GIT_VERSIONS_REPO_TOKEN:=}
: ${DEPLOY_GIT_VERSIONS_REPO_PASSWORD:="$DEPLOY_GIT_VERSIONS_REPO_TOKEN"}
: ${DEPLOY_GIT_VERSIONS_REPO_SERVER:=}
: ${DEPLOY_GIT_VERSIONS_REPO_PATH:=}
: ${DEPLOY_GIT_VERSION_FILE:=}
: ${DEPLOY_GIT_VERSION_KEY:=}
: ${DEPLOY_GIT_VERSION:=}
: ${CI_COMMIT_TAG:=}
: ${CI_PIPELINE_ID:=}

##############
# ASSERTIONS #
##############

if ! [[ "${DEPLOY_GIT_VERSIONS_REPO_PROTO}" =~ ^https?$ ]]; then
	echo "ERROR: Only HTTP or HTTPS allowed for git repos"
  exit 1
fi

if [ -z "${DEPLOY_GIT_VERSIONS_REPO_PASSWORD}" ]; then
  echo "ERROR: Password for repo cannot be empty"
  exit 1
fi

if [ -z "${DEPLOY_GIT_VERSIONS_REPO_SERVER}" ]; then
  echo "ERROR: Git server cannot be empty. Example: github.com"
  exit 1
fi

if [[ "${DEPLOY_GIT_VERSIONS_REPO_SERVER}" =~ :// ]]; then
  echo "ERROR: Git server must not include scheme. Example: github.com, not https://github.com"
  exit 1
fi

if [ -z "${DEPLOY_GIT_VERSIONS_REPO_PATH}" ]; then
  echo "ERROR: Git repo path cannot be empty. Example: /company/project/versions.git"
  exit 1
fi

if [ -z "${DEPLOY_GIT_VERSION_FILE}" ]; then
  echo "ERROR: Version filename cannot be empty. Example: production.yaml"
  exit 1
fi

if [ -z "${DEPLOY_GIT_VERSION_KEY}" ]; then
  echo "ERROR: Version key cannot be empty. Set 'DEPLOY_GIT_VERSION_KEY'. Example: 'backend-version'"
  exit 1
fi

#############
# EXECUTION #
#############

# Functions
function guess_version() {
    if [ -n "${CI_COMMIT_TAG}" ]; then
      DEPLOY_VERSION="${CI_COMMIT_TAG}"
    else
      DEPLOY_VERSION="${CI_PIPELINE_ID}"
    fi
}

function change_version() {
    git pull
    touch ${DEPLOY_GIT_VERSION_FILE}
    yq -i ".\"${DEPLOY_GIT_VERSION_KEY}\" = \"${DEPLOY_VERSION}\"" "${DEPLOY_GIT_VERSION_FILE}"
    git add ${DEPLOY_GIT_VERSION_FILE}
    git commit -m "${DEPLOY_GIT_VERSION_FILE}: ${DEPLOY_GIT_VERSION_KEY} changed to ${DEPLOY_VERSION}"
}

function push_changes() {
    git push --push-option=ci.skip
}

# Configure git
git config --global user.email "${DEPLOY_GIT_EMAIL}"
git config --global user.name "${DEPLOY_GIT_NAME}"
git config --global pull.ff only

# Clone version repo
git clone ${DEPLOY_GIT_VERSIONS_REPO_PROTO}://${DEPLOY_GIT_VERSIONS_REPO_USERNAME}:${DEPLOY_GIT_VERSIONS_REPO_PASSWORD}@${DEPLOY_GIT_VERSIONS_REPO_SERVER}/${DEPLOY_GIT_VERSIONS_REPO_PATH} versions
cd versions

if [ -n "${DEPLOY_GIT_VERSION}" ]; then
  DEPLOY_VERSION="${DEPLOY_GIT_VERSION}"
else
  guess_version
fi


i=0
while sleep $i
do
  : $((i++))
  if [ "${i}" -gt "10" ]; then
    echo "PUSH FAILED: call OPS"
    exit 1
  fi
  echo "MAKE/PUSH CHANGES: attempt $i"
  change_version
  if push_changes; then
    exit 0
  else
    git reset HEAD~1
    git clean -fxd
    git restore .
  fi
done
